function newUserCreator() {
    const userName = prompt('ВВедите имя', "Аноним");
    const userLastName = prompt('Введите фамилию', 'неизвестна');
    const userBirthDay = prompt('Введите свой возсраст в формате ДД.ММ.ГГГГ.', '01,01,1970')
    const newUser = {
        _name: userName,
        _surname: userLastName,
        _birthday: userBirthDay,
        _age: getAge(userBirthDay),
        get Login() { return (this._name[0] + this._surname).toLowerCase(); },
        get Password() { return (this._name[0].toUpperCase() + this._surname.toLowerCase() + this._age); },

    }
    Object.defineProperty(newUser, '_name', {
        value: userName,
        writable: false,
        configurable: false,
        enumarable: false,

    })
    Object.defineProperty(newUser, '_surname', {
        value: userLastName,
        writable: false,
        configurable: false,
        enumarable: false,

    })
    function getAge(Birthday) {
        const today = new Date();
        let userBirthday = ((Birthday.split('.')).reverse()).join(',');
        userBirthday = new Date(userBirthday);
        const todayYear = today.getFullYear();
        const todayMonth = today.getMonth();
        const todayDate = today.getDate();
        const userBirthdayYear = userBirthday.getFullYear();
        const userBirthdayMonth = userBirthday.getMonth() - 1;
        const userBirthdayDate = userBirthday.getDate();

        const userAge = todayYear - userBirthdayYear;

        if (todayMonth > userBirthdayMonth) return userAge;
        else if (todayMonth < userBirthdayMonth) return userAge - 1;
        else if (todayMonth === userBirthdayMonth) {
            if (userBirthdayDate >= todayDate) return userAge;
            else return userAge - 1;
        }
    }
    console.log(newUser.Login);
    console.log(newUser.Password);
    return newUser;
}

console.log(newUserCreator());


// function UserConstructor(name, surname) {
//     this._name = name;
//     this._lastName = surname;
//     this.login = (this._name[0] + this._lastName).toLowerCase();
//     Object.defineProperty(this, "_name", {
//         value: name,
//         writable: false,
//         configurable: false,
//         enumarable: false,
//     })
//     Object.defineProperty(this, "_lastName", {
//         value: surname,
//         writable: false,
//         configurable: false,
//         enumarable: false,
//     })

// }

// let myUser = new UserConstructor('Hanna', 'Khvostova');
// console.log(myUser);

