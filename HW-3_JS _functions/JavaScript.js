let a;
let b;

outer: do {
    do {
        a = prompt('Пожалуйста, введите ПЕРВОЕ число');
        if (a === null) {
            break outer;
        }
        else if (!Number.isFinite(+a)) { alert('Вы ввели не число! Введите ПЕРВОЕ число корректно!'); }
    }
    while (!Number.isFinite(+a))
    a = +a;
    do {
        b = prompt('Пожалуйста, введите ВТОРОЕ число');
        if (b === null) {
            break outer;
        }
        else if (!Number.isFinite(+b)) { alert('Вы ввели не число! Введите ВТОРОЕ число корректно!'); }
    }
    while (!Number.isFinite(+b))
    b = +b;
}
while (!Number.isFinite(a) && !Number.isFinite(b))
console.log(a, b);
if (a === undefined || b === undefined || a === null || b === null) {
    alert('Дальнейшие вычисления невозможны. Введите числа заново.')
}
else {
    const calcSumm = (a, b) => a + b;
    const calcDifference = (a, b) => a - b;
    const calcDivision = (a, b) => a / b;
    const calcMultiplication = (a, b) => a * b;
    const c = prompt('ВВедите желаемую операцию:" + " сложение; " - "вычитание ; " / " деление; " * " умножение.');
    switch (c) {
        case '+':
            alert(`Сумма ${a} + ${b} = ${calcSumm(a, b)} `);
            break;
        case '-':
            alert(`Разница ${a} - ${b} = ${calcDifference(a, b)} `);
            break;
        case '/':
            alert(`Деление ${a} / ${b} = ${calcDivision(a, b)} `);
            break;
        case '*':
            alert(`Произведение ${a} * ${b} = ${calcMultiplication(a, b)} `);
            break;
        default:
            alert('Такой операции не существует');
    }
}