do {
    number = prompt('Пожалуйста, введите ЦЕЛОЕ число', '');
    if (number === null) {
        break;
    }
    else if (number === '') { alert('Вы ничего не ввели! Введите число корректно!'); }
    else if (!Number.isFinite(+number)) { alert('Вы ввели не число! Введите число корректно!'); }
    else if (!Number.isInteger(+number)) { alert('Вы ввели НЕ ЦЕЛОЕ ЧИСЛО! Введите число корректно!'); }
}
while (!Number.isFinite(+number) || !Number.isInteger(+number) || number === '')
number = +number;


function getFibonachi(n) {
    let x0 = 0;
    let x1 = 1;
    let x2;
    if (n == 0) { return x0; }
    else if (n == 1 || n === -1) { return x1; }
    else if (n >= 2) {
        for (let i = 2; i <= n; i++) {
            x2 = x0 + x1;
            x0 = x1;
            x1 = x2;
        }
        return x2;
    }
    else if (n <= -2) {
        for (let j = -2; j >= n; j--) {
            x2 = x0 - x1;
            x0 = x1;
            x1 = x2;
        }
        return x2;
    }
}
alert(`В числовой последовательности Фибонначи под порядковым номером ${number} находится число ${getFibonachi(number)}`);

// --------------------------------------------------------------------------

function findFibonachi(n) {
    if (n < 2 && n >= 0) {
        return n;
    } else if (n >= 2) {
        return findFibonachi(n - 1) + findFibonachi(n - 2);
    }
    else if (n < 0) {
        return findFibonachi(n + 2) - findFibonachi(n + 1);
    }
}
alert(`В числовой последовательности Фибонначи под порядковым номером ${number} находится число ${findFibonachi(number)}`);