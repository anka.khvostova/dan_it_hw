function filterBy(array, type) {
    return array.filter(item => (typeof item) != type)

}
let arr = [6, 7, 10, 1, 23, 'Anna', 6, null, 'someString', true];
console.log(filterBy(arr, 'number'));
